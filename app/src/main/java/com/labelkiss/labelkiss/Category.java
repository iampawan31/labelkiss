package com.labelkiss.labelkiss;

import android.app.ActionBar;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.api.client.auth.oauth.OAuthParameters;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.labelkiss.labelkiss.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;


public class Category extends NavigationActivity {

    private ProgressBar progress;
    TextView serverRespText;
    ArrayList<GridList> actorsList;
    DiscoverAdapter adapter;
    String category;
    String urlconstant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.first_category, frameLayout);

        ActionBar mActionBar;
        mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        LayoutInflater mInflater = LayoutInflater.from(this);


        //View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        //View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setCustomView(R.layout.custom_actionbar);


        //mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeButtonEnabled(true);

        progress = (ProgressBar) findViewById(R.id.progressBar);
        Resources res = getResources();
        progress.setProgressDrawable(res.getDrawable(R.drawable.progress_bar));
        progress.setVisibility(View.VISIBLE);
        progress.setIndeterminate(true);
        progress.setProgress(0);

        Intent intent = getIntent();
        category = intent.getStringExtra("category");
           if(category.equals("1.1"))
   	     {
             urlconstant=Constants.PRODUCT_API_REQUEST_FIRSTCATEGORY_FIRST;

   	     }
        else if(category.equals("1.2"))
        {
            urlconstant=Constants.PRODUCT_API_REQUEST_FIRSTCATEGORY_SECOND;

         }
        else if(category.equals("1.3"))
       {
           urlconstant=Constants.PRODUCT_API_REQUEST;

        }
           else if(category.equals("2.1"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_SECONDCATEGORY_FIRST;

           }
           else if(category.equals("2.2"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_SECONDCATEGORY_SECOND;

           }
           else if(category.equals("2.3"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_SECONDCATEGORY_THIRD;

           }
           else if(category.equals("2.4"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_SECONDCATEGORY_FOURTH;

           }
           else if(category.equals("2.5"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_SECONDCATEGORY_FIFTH;

           }
           else if(category.equals("2.6"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_SECONDCATEGORY_SIXTH;

           }
           else if(category.equals("2.7"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_SECONDCATEGORY_SEVENTH;

           }
           else if(category.equals("3.1"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_THIRDCATEGORY_FIRST;

           }
           else if(category.equals("3.2"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_THIRDCATEGORY_SECOND;

           }
           else if(category.equals("4.1"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_FOURTHCATEGORY_FIRST;

           }
           else if(category.equals("4.2"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_FOURTHCATEGORY_SECOND;

           }
           else if(category.equals("5.1"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_FIFTHCATEGORY_FIRST;

           }
           else if(category.equals("5.2"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_FIFTHCATEGORY_SECOND;

           }
           else if(category.equals("5.3"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_FIFTHCATEGORY_THIRD;

           }
           else if(category.equals("5.4"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_FIFTHCATEGORY_FOURTH;

           }
           else if(category.equals("6.1"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_SIXTHCATEGORY_FIRST;

           }
           else if(category.equals("6.2"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_SIXTHCATEGORY_SECOND;

           }
           else if(category.equals("7.1"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_SEVENCATEGORY_FIRST;

           }
           else if(category.equals("2.2"))
           {
               urlconstant=Constants.PRODUCT_API_REQUEST_SEVENCATEGORY_SECOND;

           }
        else
           {
               urlconstant=Constants.PRODUCT_API_REQUEST;
           }





        actorsList = new ArrayList<GridList>();

        GridView grid = (GridView) findViewById(R.id.discover_grid_view);
        adapter = new DiscoverAdapter(Category.this, R.layout.grid_item, actorsList);

        grid.setAdapter(adapter);
        grid.setBackgroundColor(Color.parseColor("#E1E2E3"));
        grid.setVerticalSpacing(1);
        grid.setHorizontalSpacing(1);

        new DownloadJson(urlconstant, getConsumer()).execute();

        // launching pdfviewer activity
        grid.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {


                String productid= actorsList.get(position).getfid();

                // Starting new intent
                Intent in = new Intent(Category.this,
                        EProductDesc.class);
                in.putExtra("productid", productid);

                startActivity(in);
            }
        });



    }


    // DownloadJSON AsyncTask
    private class DownloadJson extends AsyncTask {
        private String url;
        private OAuthParameters consumer;
        public HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

        public DownloadJson(String apiRequest, OAuthParameters consumer) {
            this.url = apiRequest;
            this.consumer = consumer;
        }

        @Override
        protected Object doInBackground(Object[] params) {
            return sendGoogleApiRequest();
        }


        private Object sendGoogleApiRequest() {
            try {

                GenericUrl requestUrl = new GenericUrl(url);

                HttpRequestFactory requestFactory = HTTP_TRANSPORT
                        .createRequestFactory(new HttpRequestInitializer() {
                            @Override
                            public void initialize(HttpRequest request) {

                                request.getHeaders().setAccept("application/json");
                            }
                        });

                HttpRequest request = requestFactory.buildGetRequest(requestUrl);

                consumer.initialize(request);


                Log.d(TAG, "Calling server with url:" + url);
                HttpResponse response = request.execute();
                Log.d(TAG, request.getHeaders().getAuthorization());
                if (response.isSuccessStatusCode()) {
                    return response.parseAsString();
                } else {
                    Log.w(TAG, "Issue with the server call: " + response.getStatusMessage());
                }

            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            progress.setProgress(100);


            adapter.notifyDataSetChanged();
            try {
                JSONObject jsonObj = new JSONObject(String.valueOf(result));
                // searchResult refers to the current element in the array "search_result"
                // JSONObject questionMark = searchResult.getJSONObject("question_mark");
                Iterator keys = jsonObj.keys();

                while(keys.hasNext()) {
                    // loop to get the dynamic key
                    String currentDynamicKey = (String)keys.next();

                    // get the value of the dynamic key
                    JSONObject currentDynamicValue = jsonObj.getJSONObject(currentDynamicKey);



                    String id = currentDynamicValue.getString("entity_id");
                    String name = currentDynamicValue.getString("name");
                    String price = currentDynamicValue.getString("final_price_with_tax");
                    String imageUrl = currentDynamicValue.getString("image_url");
                    String shortDescription = currentDynamicValue.getString("short_description");
                    Log.v("entity_id", id);
                    Log.v("name", name);
                    Log.v("meta_title", price);
                    Log.v("imageUrl", imageUrl);


                    GridList actor = new GridList();

                    actor.setfid(id);
                    actor.setImage(imageUrl);
                    actor.setDescription(price);
                    actor.setName(name);


                    actorsList.add(actor);


                }
                // Getting JSON Array node

                progress.setVisibility(View.GONE);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
