package com.labelkiss.labelkiss;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.api.client.auth.oauth.OAuthParameters;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.labelkiss.labelkiss.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;


public class MainActivity extends NavigationActivity {

    private ProgressBar progress;
    TextView serverRespText;
    ArrayList<GridList> actorsList;
    DiscoverAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLayoutInflater().inflate(R.layout.activity_main, frameLayout);
        //setContentView(R.layout.activity_main);

        ActionBar mActionBar;

        mActionBar = getActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));

        LayoutInflater mInflater = LayoutInflater.from(this);


        //View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        //View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setCustomView(R.layout.custom_actionbar);


        //mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeButtonEnabled(true);
        Button startedBtn = (Button) findViewById(R.id.login_btn_get_started);
        Button catalogBtn = (Button) findViewById(R.id.catalog_btn);
        serverRespText = (TextView) findViewById(R.id.server_response);

        actorsList = new ArrayList<GridList>();

        GridView grid = (GridView) findViewById(R.id.discover_grid_view);
        adapter = new DiscoverAdapter(MainActivity.this, R.layout.grid_item, actorsList);

        grid.setAdapter(adapter);

        //Hide the button based on authtoken
        if (localCredentialStore.getToken().getAuthToken().isEmpty()) {
            startedBtn.setVisibility(View.VISIBLE);
            catalogBtn.setVisibility(View.GONE);
        } else {
            startedBtn.setVisibility(View.GONE);
            catalogBtn.setVisibility(View.VISIBLE);
        }
        //Open the webview to allow user to access the login page
        startedBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent().setClass(v.getContext(), WebActivity.class));
            }
        });

        //Fetch the json objects after authorization
        catalogBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                progress.setVisibility(View.VISIBLE);
                serverRespText.setText("");
                new DownloadJson(Constants.PRODUCT_API_REQUEST, getConsumer()).execute();
            }
        });
        progress = (ProgressBar) findViewById(R.id.progressBar);
        int color = 0xFF00FF00;
        progress.getIndeterminateDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        progress.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
        progress.setVisibility(View.GONE);
        progress.setIndeterminate(true);
        progress.setProgress(0);
    }


    // DownloadJSON AsyncTask
    private class DownloadJson extends AsyncTask {
        private String url;
        private OAuthParameters consumer;
        public HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

        public DownloadJson(String apiRequest, OAuthParameters consumer) {
            this.url = apiRequest;
            this.consumer = consumer;
        }

        @Override
        protected Object doInBackground(Object[] params) {
            return sendGoogleApiRequest();
        }


        private Object sendGoogleApiRequest() {
            try {

                GenericUrl requestUrl = new GenericUrl(url);

                HttpRequestFactory requestFactory = HTTP_TRANSPORT
                        .createRequestFactory(new HttpRequestInitializer() {
                            @Override
                            public void initialize(HttpRequest request) {

                                request.getHeaders().setAccept("application/json");
                            }
                        });

                HttpRequest request = requestFactory.buildGetRequest(requestUrl);

                consumer.initialize(request);


                Log.d(TAG, "Calling server with url:" + url);
                HttpResponse response = request.execute();
                Log.d(TAG, request.getHeaders().getAuthorization());
                if (response.isSuccessStatusCode()) {
                    return response.parseAsString();
                } else {
                    Log.w(TAG, "Issue with the server call: " + response.getStatusMessage());
                }

            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            progress.setProgress(100);
            progress.setVisibility(View.GONE);
            Log.v(TAG, String.valueOf(result));
            serverRespText.setText(String.valueOf(result));
            adapter.notifyDataSetChanged();
            try {
                JSONObject jsonObj = new JSONObject(String.valueOf(result));
                // searchResult refers to the current element in the array "search_result"
               // JSONObject questionMark = searchResult.getJSONObject("question_mark");
                Iterator keys = jsonObj.keys();

                while(keys.hasNext()) {
                    // loop to get the dynamic key
                    String currentDynamicKey = (String)keys.next();

                    // get the value of the dynamic key
                    JSONObject currentDynamicValue = jsonObj.getJSONObject(currentDynamicKey);



                        String id = currentDynamicValue.getString("entity_id");
                        String name = currentDynamicValue.getString("name");
                        String price = currentDynamicValue.getString("final_price_with_tax");
                        String imageUrl = currentDynamicValue.getString("image_url");
                        String shortDescription = currentDynamicValue.getString("short_description");
                        Log.v("entity_id", id);
                        Log.v("name", name);
                        Log.v("meta_title", price);
                        Log.v("imageUrl", imageUrl);


                    GridList actor = new GridList();

                    actor.setfid(id);
                    actor.setImage(imageUrl);
                    actor.setDescription(price);
                    actor.setName(name);


                    actorsList.add(actor);


                }
                // Getting JSON Array node



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
