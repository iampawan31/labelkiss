package com.labelkiss.labelkiss;


public class GridList {


    private String fid;
    private String name;
    private String description;
    private String image;



    public GridList() {
        // TODO Auto-generated constructor stub
    }

    public GridList(String name,String fid, String description ,String image) {
        super();
        this.name = name;
        this.fid = fid;
        this.description = description;
        this.image = image;


    }


    public String getName() {
        return name;
    }



    public void setName(String name) {
        this.name = name;
    }


    public String getfid() {
        return fid;
    }

    public void setfid(String fid) {
        this.fid = fid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }



}
