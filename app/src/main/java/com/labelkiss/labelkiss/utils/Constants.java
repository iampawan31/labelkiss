package com.labelkiss.labelkiss.utils;


public class Constants {

	//You Custom Consumer Key
	public static final String CONSUMER_KEY = "3435f9662f77db9ce91216a009e09b85";
	//You Custom Consumer SECRET
	public static final String CONSUMER_SECRET = "3e049ee797b64eff55765e8c29c101d4";
	//Your Base URL for the site
	public static final String BASE_URL = "https://confidentialcouture.com/";

	public static final String REQUEST_URL 		= BASE_URL + "oauth/initiate";
	public static final String ACCESS_URL 		= BASE_URL + "oauth/token";
	public static final String AUTHORIZE_URL 	= BASE_URL + "oauth/authorize";
	public static final String API_REQUEST 		= BASE_URL + "api/rest/";

	public static final String PRODUCT_API_REQUEST 		=   API_REQUEST+"cmsPage/1";
	public static final String PRODUCT_API_REQUEST_FIRST 		=   API_REQUEST+"products?category_id=10&limit=20";


	public static final String PRODUCT_API_REQUEST_FIRSTCATEGORY_FIRST 		=   API_REQUEST+"products?category_id=6&limit=20";
	public static final String PRODUCT_API_REQUEST_FIRSTCATEGORY_SECOND 		=   API_REQUEST+"products?category_id=6&limit=20";
	public static final String PRODUCT_API_REQUEST_FIRSTCATEGORY_THIRD		=   API_REQUEST+"products?category_id=6&limit=20";

	public static final String PRODUCT_API_REQUEST_SECONDCATEGORY_FIRST 		=   API_REQUEST+"products?category_id=74&limit=20";
	public static final String PRODUCT_API_REQUEST_SECONDCATEGORY_SECOND 		=   API_REQUEST+"products?category_id=74&limit=20";
	public static final String PRODUCT_API_REQUEST_SECONDCATEGORY_THIRD		=   API_REQUEST+"products?category_id=74&limit=20";
	public static final String PRODUCT_API_REQUEST_SECONDCATEGORY_FOURTH		=   API_REQUEST+"products?category_id=74&limit=20";
	public static final String PRODUCT_API_REQUEST_SECONDCATEGORY_FIFTH		=   API_REQUEST+"products?category_id=74&limit=20";
	public static final String PRODUCT_API_REQUEST_SECONDCATEGORY_SIXTH		=   API_REQUEST+"products?category_id=74&limit=20";
	public static final String PRODUCT_API_REQUEST_SECONDCATEGORY_SEVENTH		=   API_REQUEST+"products?category_id=74&limit=20";
	public static final String PRODUCT_API_REQUEST_SECONDCATEGORY_EIGHT		=   API_REQUEST+"products?category_id=74&limit=20";


	public static final String PRODUCT_API_REQUEST_THIRDCATEGORY_FIRST 		=   API_REQUEST+"products?category_id=10&limit=20";
	public static final String PRODUCT_API_REQUEST_THIRDCATEGORY_SECOND 		=   API_REQUEST+"products?category_id=12&limit=20";
	public static final String PRODUCT_API_REQUEST_THIRDCATEGORY_THIRD		=   API_REQUEST+"products?category_id=70&limit=20";
	public static final String PRODUCT_API_REQUEST_THIRDCATEGORY_FOURTH		=   API_REQUEST+"products?category_id=71&limit=20";
	public static final String PRODUCT_API_REQUEST_THIRDCATEGORY_FIFTH		=   API_REQUEST+"products?category_id=72&limit=20";
	public static final String PRODUCT_API_REQUEST_THIRDCATEGORY_SIXTH		=   API_REQUEST+"products?category_id=73&limit=20";
	public static final String PRODUCT_API_REQUEST_THIRDCATEGORY_SEVEN		=   API_REQUEST+"products?category_id=74&limit=20";
	public static final String PRODUCT_API_REQUEST_THIRDCATEGORY_EIGHT		=   API_REQUEST+"products?category_id=75&limit=20";

	public static final String PRODUCT_API_REQUEST_FOURTHCATEGORY_FIRST 		=   API_REQUEST+"products?category_id=68&limit=20";
	public static final String PRODUCT_API_REQUEST_FOURTHCATEGORY_SECOND 		=   API_REQUEST+"products?category_id=68&limit=20";
	public static final String PRODUCT_API_REQUEST_FOURTHCATEGORY_THIRD		=   API_REQUEST+"products?category_id=25&limit=20";
	public static final String PRODUCT_API_REQUEST_FOURTHCATEGORY_FOURTH		=   API_REQUEST+"products?category_id=26&limit=20";


	public static final String PRODUCT_API_REQUEST_FIFTHCATEGORY_FIRST 		=   API_REQUEST+"products?category_id=4&limit=20";
	public static final String PRODUCT_API_REQUEST_FIFTHCATEGORY_SECOND 		=   API_REQUEST+"products?category_id=12&limit=20";
	public static final String PRODUCT_API_REQUEST_FIFTHCATEGORY_THIRD		=   API_REQUEST+"products?category_id=20&limit=20";
	public static final String PRODUCT_API_REQUEST_FIFTHCATEGORY_FOURTH		=   API_REQUEST+"products?category_id=44&limit=20";

	public static final String PRODUCT_API_REQUEST_SIXTHCATEGORY_FIRST		=   API_REQUEST+"products?category_id=4&limit=20";
	public static final String PRODUCT_API_REQUEST_SIXTHCATEGORY_SECOND		=   API_REQUEST+"products?category_id=12&limit=20";

	public static final String PRODUCT_API_REQUEST_SEVENCATEGORY_FIRST		=   API_REQUEST+"products?category_id=79&limit=20";
	public static final String PRODUCT_API_REQUEST_SEVENCATEGORY_SECOND		=   API_REQUEST+"products?category_id=79&limit=20";

	public static final String PRODUCT_API_REQUEST_GET_DESCRIPTION	=   API_REQUEST+"products/";

	public static final String ENCODING 		= "UTF-8";

	public static final String OAUTH_CALLBACK_URL = "http://localhost/";

}
