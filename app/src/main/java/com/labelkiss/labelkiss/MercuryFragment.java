package com.labelkiss.labelkiss;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class MercuryFragment extends Fragment{

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.first, null);


		ActionBar mActionBar;
		mActionBar = getActivity().getActionBar();
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));

		LayoutInflater mInflater = LayoutInflater.from(getActivity());


		//View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
		//View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		mActionBar.setCustomView(R.layout.custom_actionbar);


		//mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);

		//mActionBar.setHomeAsUpIndicator(R.drawable.menu_icon);




		return rootView;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
			case android.R.id.home:

				break;
			default:
				return super.onOptionsItemSelected(item);
		}
		return true;
	}
}
