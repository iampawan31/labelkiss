package com.labelkiss.labelkiss;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ECategoryAdapter extends ArrayAdapter<ECategoryList> {
	ArrayList<ECategoryList> actorList;
	LayoutInflater vi;
	int Resource;
	ViewHolder holder;

	public ECategoryAdapter(Context context, int resource, ArrayList<ECategoryList> objects) {
		super(context, resource, objects);
		vi = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Resource = resource;
		actorList = objects;
		
		
		
	}
 
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// convert view = design
		View v = convertView;
		

				

		
		if (v == null) {
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.imageview = (ImageView) v.findViewById(R.id.bannersize);
			holder.tvName = (Button) v.findViewById(R.id.button);
			Typeface custom_font2 = Typeface.createFromAsset(getContext().getAssets(),  "fonts/Majesti-Banner-Book.otf");
			holder.tvName.setTypeface(custom_font2);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}


		  Picasso.with(getContext())
          .load(actorList.get(position).getImage())
          .fit()
          .centerCrop()
          .into(holder.imageview);
		holder.tvName.setText(actorList.get(position).getName());

		return v;

	}

	static class ViewHolder {

		public ImageView imageview;
		public Button tvName;


	}



}