package com.labelkiss.labelkiss;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;



 class DiscoverAdapter extends ArrayAdapter<GridList> {

    ArrayList<GridList> actorList;
    LayoutInflater vi;
    int Resource;
    ViewHolder holder = null;




    public DiscoverAdapter(Context context, int resource, ArrayList<GridList> objects) {
        super(context, resource, objects);
        vi = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        Resource = resource;
        actorList = objects;



    }


    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // convert view = design

        System.out.println("getview:"+position+" "+convertView);
        View v = convertView;





        if (v == null) {
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);





            holder.disimage = (ImageView) v.findViewById(R.id.discoverimage);
            holder.disusername = (TextView) v.findViewById(R.id.username);
            holder.disdescription = (TextView) v.findViewById(R.id.discoverdesc);




            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }


        String Filename =actorList.get(position).getImage().replace(" ", "%20");



        Picasso.with(getContext())
                .load(Filename)
                .into(holder.disimage);



        holder.disusername.setText(actorList.get(position).getName());
        Typeface custom_font2 = Typeface.createFromAsset(getContext().getAssets(),  "fonts/Raleway-SemiBold.ttf");
        holder.disusername.setTypeface(custom_font2);

        holder.disdescription.setText("₹"+actorList.get(position).getDescription());
        Typeface custom_font = Typeface.createFromAsset(getContext().getAssets(),  "fonts/Raleway-Regular.ttf");
        holder.disdescription.setTypeface(custom_font);




        return v;


    }

    static class ViewHolder {

        public ImageView disimage;
        public TextView disusername;
        public TextView disdescription;





    }



}