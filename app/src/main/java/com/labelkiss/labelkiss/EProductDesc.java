package com.labelkiss.labelkiss;

import android.app.ActionBar;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.api.client.auth.oauth.OAuthParameters;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.labelkiss.labelkiss.utils.Constants;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class EProductDesc extends BaseActivity {
	

	 private ProgressBar progress;
	 String productid;
	 TextView pprice;
	 TextView pname;
	ImageView productimg;
	Button productidd;


	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_desc);

		ActionBar mActionBar;
		mActionBar = getActionBar();
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));

		LayoutInflater mInflater = LayoutInflater.from(this);
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		mActionBar.setCustomView(R.layout.custom_actionbar);
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);

		Intent intent = getIntent();
		Bundle bundle = intent.getExtras();
		productid= bundle.getString("productid");



//		urlconstant= Constants.PRODUCT_API_REQUEST+"/"+8;


		progress = (ProgressBar) findViewById(R.id.progressBar);
		Resources res = getResources();
		progress.setProgressDrawable(res.getDrawable(R.drawable.progress_bar));
		progress.setVisibility(View.VISIBLE);
		progress.setIndeterminate(true);
		progress.setProgress(0);
        
        
        
        pprice = (TextView)findViewById(R.id.productprice);
		pname = (TextView)findViewById(R.id.productname);
        productimg = (ImageView)findViewById(R.id.productimage);



//        Button productidd =  (Button)findViewById(R.id.buybutton);
//        Typeface font2 = Typeface.createFromAsset(getAssets(),
//		        "AvenirNextLTPro-Regular.otf");
//        productidd.setTypeface(font2);

		new DownloadJson(Constants.PRODUCT_API_REQUEST_GET_DESCRIPTION+productid, getConsumer()).execute();
        

        

}


	// DownloadJSON AsyncTask
	private class DownloadJson extends AsyncTask {
		private String url;
		private OAuthParameters consumer;
		public HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

		public DownloadJson(String apiRequest, OAuthParameters consumer) {
			this.url = apiRequest;
			this.consumer = consumer;
		}

		@Override
		protected Object doInBackground(Object[] params) {
			return sendGoogleApiRequest();
		}


		private Object sendGoogleApiRequest() {
			try {

				GenericUrl requestUrl = new GenericUrl(url);

				HttpRequestFactory requestFactory = HTTP_TRANSPORT
						.createRequestFactory(new HttpRequestInitializer() {
							@Override
							public void initialize(HttpRequest request) {

								request.getHeaders().setAccept("application/json");
							}
						});

				HttpRequest request = requestFactory.buildGetRequest(requestUrl);

				consumer.initialize(request);


				Log.d(TAG, "Calling server with url:" + url);
				HttpResponse response = request.execute();
				Log.d(TAG, request.getHeaders().getAuthorization());
				if (response.isSuccessStatusCode()) {
					return response.parseAsString();
				} else {
					Log.w(TAG, "Issue with the server call: " + response.getStatusMessage());
				}

			} catch (Exception e) {
				Log.e(TAG, e.getMessage(), e);
			}

			return null;
		}


		protected void onPostExecute(Object result) {
			super.onPostExecute(result);
			progress.setProgress(100);

			try {
				JSONObject jsonObj = new JSONObject(String.valueOf(result));
				System.out.println(result);
				String id = jsonObj.getString("entity_id");
				String name = jsonObj.getString("name");
				String price = jsonObj.getString("regular_price_without_tax");
				String imageUrl = jsonObj.getString("image_url");
				String shortDescription = jsonObj.getString("short_description");
				String buyurl = jsonObj.getString("buy_now_url");
				String stock = jsonObj.getString("is_in_stock");
				String sku = jsonObj.getString("sku");

				Log.v("entity_id", id);
 			    Log.v("name", name);
 				Log.v("meta_title", price);
				Log.v("buyurl", buyurl);


				pprice.setText("₹"+ " " + price);
				Typeface custom_font2 = Typeface.createFromAsset(getAssets(),  "fonts/Raleway-Regular.ttf");
				pprice.setTypeface(custom_font2);
				pname.setText(name);
				Typeface custom_font = Typeface.createFromAsset(getAssets(),  "fonts/Raleway-SemiBold.ttf");
				pname.setTypeface(custom_font);

				Picasso.with(getApplicationContext())
						.load(imageUrl)
						.into(productimg);

				//productidd.setTag(buyurl);


				progress.setVisibility(View.GONE);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}
	
	
}