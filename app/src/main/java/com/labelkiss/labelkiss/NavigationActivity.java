package com.labelkiss.labelkiss;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class NavigationActivity extends BaseActivity {

    private DrawerLayout mDrawerLayout;
    protected FrameLayout frameLayout;
    ImageView home;
    Fragment fragment = null;
    TextView appname;
    protected ExpandableListView expListView;
    HashMap<String, List<String>> listDataChild;
    ExpandableListAdapter listAdapter;
    List<String> listDataHeader;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        setUpDrawer();


    }

    /**
     * Get the names and icons references to build the drawer menu...
     */
    private void setUpDrawer() {
        ActionBar mActionBar;
        mActionBar = getActionBar();
        //mActionBar.setHomeAsUpIndicator(R.drawable.menu_icon);
        mActionBar.setHomeButtonEnabled(true);
        mActionBar.setDisplayHomeAsUpEnabled(true);

        frameLayout = (FrameLayout) findViewById(R.id.content_frame);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
        mDrawerLayout.setDrawerListener(mDrawerListener);


        actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.menu_icon, R.string.open_drawer,
                R.string.close_drawer) {
            public void onDrawerClosed(View view) {
                getActionBar().setSubtitle("open");
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                getActionBar().setSubtitle("close");
            }

        };

        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);
        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        prepareListData();
        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);
        //fragment = new MercuryFragment();

        //getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
        mDrawerLayout.closeDrawer(expListView);

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.header_sidebar, expListView,
                false);


        ImageView  headerimage = (ImageView)header.findViewById(R.id.imageView1);
        headerimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent catone = new Intent(NavigationActivity.this, ECategories.class);
                NavigationActivity.this.startActivity(catone);
            }
        });

        expListView.addHeaderView(header, null, false);


        expListView.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                switch (groupPosition) {
                    case 0:
                        switch (childPosition) {
                            case 0:
                                Intent catone = new Intent(NavigationActivity.this, Category.class);
                                catone.putExtra("category", "1.1");
                                NavigationActivity.this.startActivity(catone);
                                break;
                            case 1:
                                Intent cattwo = new Intent(NavigationActivity.this, Category.class);
                                cattwo.putExtra("category", "1.2");
                                NavigationActivity.this.startActivity(cattwo);
                                break;

                            default:
                                break;
                        }
                        break;

                    case 1:
                        switch (childPosition) {
                            case 0:
                                Intent catone = new Intent(NavigationActivity.this, Category.class);
                                catone.putExtra("category", "2.1");
                                NavigationActivity.this.startActivity(catone);
                                break;
                            case 1:
                                Intent cattwo = new Intent(NavigationActivity.this, Category.class);
                                cattwo.putExtra("category", "2.2");
                                NavigationActivity.this.startActivity(cattwo);
                                break;

                            case 2:
                                Intent catthree = new Intent(NavigationActivity.this, Category.class);
                                catthree.putExtra("category", "2.3");
                                NavigationActivity.this.startActivity(catthree);
                                break;
                            case 3:
                                Intent catfour = new Intent(NavigationActivity.this, Category.class);
                                catfour.putExtra("category", "2.4");
                                NavigationActivity.this.startActivity(catfour);
                                break;
                            case 4:
                                Intent catfive = new Intent(NavigationActivity.this, Category.class);
                                catfive.putExtra("category", "2.5");
                                NavigationActivity.this.startActivity(catfive);
                                break;
                            case 5:
                                Intent catsix = new Intent(NavigationActivity.this, Category.class);
                                catsix.putExtra("category", "2.6");
                                NavigationActivity.this.startActivity(catsix);
                                break;
                            case 6:
                                Intent catseven = new Intent(NavigationActivity.this, Category.class);
                                catseven.putExtra("category", "2.7");
                                NavigationActivity.this.startActivity(catseven);
                                break;


                            default:
                                break;
                        }
                        break;

                    case 2:
                        switch (childPosition) {
                            case 0:
                                Intent catone = new Intent(NavigationActivity.this, Category.class);
                                catone.putExtra("category", "3.1");
                                NavigationActivity.this.startActivity(catone);
                                break;
                            case 1:
                                Intent cattwo = new Intent(NavigationActivity.this, Category.class);
                                cattwo.putExtra("category", "3.2");
                                NavigationActivity.this.startActivity(cattwo);
                                break;

                            default:
                                break;
                        }
                        break;
                    case 3:
                        switch (childPosition) {
                            case 0:
                                Intent catone = new Intent(NavigationActivity.this, Category.class);
                                catone.putExtra("category", "4.1");
                                NavigationActivity.this.startActivity(catone);
                                break;
                            case 1:
                                Intent cattwo = new Intent(NavigationActivity.this, Category.class);
                                cattwo.putExtra("category", "4.2");
                                NavigationActivity.this.startActivity(cattwo);
                                break;

                            default:
                                break;
                        }
                        break;
                    case 4:
                        switch (childPosition) {
                            case 0:
                                Intent catone = new Intent(NavigationActivity.this, Category.class);
                                catone.putExtra("category", "5.1");
                                NavigationActivity.this.startActivity(catone);
                                break;
                            case 1:
                                Intent cattwo = new Intent(NavigationActivity.this, Category.class);
                                cattwo.putExtra("category", "5.2");
                                NavigationActivity.this.startActivity(cattwo);
                                break;
                            case 2:
                                Intent catthree = new Intent(NavigationActivity.this, Category.class);
                                catthree.putExtra("category", "5.3");
                                NavigationActivity.this.startActivity(catthree);
                                break;
                            case 3:
                                Intent catfour = new Intent(NavigationActivity.this, Category.class);
                                catfour.putExtra("category", "5.4");
                                NavigationActivity.this.startActivity(catfour);
                                break;

                            default:
                                break;
                        }
                        break;
                    case 5:
                        switch (childPosition) {
                            case 0:
                                Intent catone = new Intent(NavigationActivity.this, Category.class);
                                catone.putExtra("category", "6.1");
                                NavigationActivity.this.startActivity(catone);
                                break;
                            case 1:
                                Intent cattwo = new Intent(NavigationActivity.this, Category.class);
                                cattwo.putExtra("category", "6.2");
                                NavigationActivity.this.startActivity(cattwo);
                                break;

                            default:
                                break;
                        }
                        break;
                    case 6:
                        switch (childPosition) {
                            case 0:
                                Intent catone = new Intent(NavigationActivity.this, Category.class);
                                catone.putExtra("category", "7.1");
                                NavigationActivity.this.startActivity(catone);
                                break;
                            case 1:
                                Intent cattwo = new Intent(NavigationActivity.this, Category.class);
                                cattwo.putExtra("category", "7.2");
                                NavigationActivity.this.startActivity(cattwo);
                                break;

                            default:
                                break;
                        }
                        break;
                    default:
                        break;
                }
                //getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();
                mDrawerLayout.closeDrawer(expListView);
                return false;
            }
        });
    }

    OnClickListener homeOnclickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mDrawerLayout.isDrawerOpen(expListView)) {
                mDrawerLayout.closeDrawer(expListView);
            } else {
                mDrawerLayout.openDrawer(expListView);
            }
        }
    };

    private OnItemClickListener mDrawerItemClickedListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

            switch (position) {
                case 0:
                    Intent hospital1 = new Intent(NavigationActivity.this, WebActivity.class);
                    NavigationActivity.this.startActivity(hospital1);
                    break;
                case 1:
                    Intent hospital2 = new Intent(NavigationActivity.this, WebActivity.class);
                    NavigationActivity.this.startActivity(hospital2);
                    break;
                case 2:
                    Intent hospital3 = new Intent(NavigationActivity.this, WebActivity.class);
                    NavigationActivity.this.startActivity(hospital3);
                    break;
                default:
                    return;
            }

            //getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();

            mDrawerLayout.closeDrawer(expListView);
        }
    };

    // Catch the events related to the drawer to arrange views according to this
// action if necessary...
    private DrawerListener mDrawerListener = new DrawerListener() {

        @Override
        public void onDrawerStateChanged(int status) {

        }

        @Override
        public void onDrawerSlide(View view, float slideArg) {

        }

        @Override
        public void onDrawerOpened(View view) {
        }

        @Override
        public void onDrawerClosed(View view) {
        }

    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("New In");
        listDataHeader.add("Jewellery");
        listDataHeader.add("Bags");
        listDataHeader.add("Sunglasses");
        listDataHeader.add("Designers");
        listDataHeader.add("Mens Accessories");
        listDataHeader.add("Kiss for Fun");


        // Adding child data
        List<String> batsman = new ArrayList<String>();
        batsman.add("Just Arrived");
        batsman.add("Kiss Picks");


        List<String> bowler = new ArrayList<String>();
        bowler.add("Earrings");
        bowler.add("Necklaces");
        bowler.add("Bracelets");
        bowler.add("Rings");
        bowler.add("Body Jewellery");
        bowler.add("Sterling Silver");
        bowler.add("Made to Order");
        bowler.add("Kiss Picks");

        List<String> all = new ArrayList<String>();
        all.add("All Bags");
        all.add("Kiss Picks");


        List<String> wk = new ArrayList<String>();
        wk.add("All Sunglasses");
        wk.add("Kiss Picks");

        List<String> design = new ArrayList<String>();
        design.add("Euremme");
        design.add("Geoielli");
        design.add("Sangeeta Boochra");
        design.add("Vasundhra");

        List<String> Mens = new ArrayList<String>();
        Mens.add("Cuff Links");
        Mens.add("Ties & Bows");

        List<String> fun = new ArrayList<String>();
        fun.add("Paper Clips");
        fun.add("Tattoos");


        listDataChild.put(listDataHeader.get(0), batsman); // Header, Child data
        listDataChild.put(listDataHeader.get(1), bowler);
        listDataChild.put(listDataHeader.get(2), all);
        listDataChild.put(listDataHeader.get(3), wk);
        listDataChild.put(listDataHeader.get(4), design);
        listDataChild.put(listDataHeader.get(5), Mens);
        listDataChild.put(listDataHeader.get(6), fun);
    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private Context _context;
        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, List<String>> _listDataChild;

        public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                     HashMap<String, List<String>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final String childText = (String) getChild(groupPosition, childPosition);

            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item, null);
            }

            TextView txtListChild = (TextView) convertView
                    .findViewById(R.id.lblListItem);

            txtListChild.setText(childText);
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return this._listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }

            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);


            lblListHeader.setText(headerTitle);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }
}
