package com.labelkiss.labelkiss;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;


public class LoginActivity extends BaseActivity {

    private ProgressBar progress;
    TextView serverRespText;
    ArrayList<GridList> actorsList;
    DiscoverAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        Button startedBtn = (Button) findViewById(R.id.login_btn_get_started);



        //Hide the button based on authtoken
        if (localCredentialStore.getToken().getAuthToken().isEmpty()) {
            startedBtn.setVisibility(View.VISIBLE);

        } else {

            Intent openMainActivity =  new Intent(LoginActivity.this, ECategories.class);
            startActivity(openMainActivity);
            finish();
        }
        //Open the webview to allow user to access the login page
        startedBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent().setClass(v.getContext(), WebActivity.class));
            }
        });



    }




}
