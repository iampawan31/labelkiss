package com.labelkiss.labelkiss;

import android.app.ProgressDialog;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import android.app.ActionBar;


public class ECategories extends NavigationActivity {
	
	ArrayList<ECategoryList> actorsList;
	JSONArray jarray;
	ECategoryAdapter adapter;
	JSONObject jsono;
	String pid;
	private ProgressBar progress;


	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getLayoutInflater().inflate(R.layout.category_list, frameLayout);
		//setContentView(R.layout.category_list);



		ActionBar mActionBar;
		mActionBar = getActionBar();
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.WHITE));
		LayoutInflater mInflater = LayoutInflater.from(this);
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		mActionBar.setCustomView(R.layout.custom_actionbar);
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(true);
		mActionBar.setHomeButtonEnabled(true);
	 

		actorsList = new ArrayList<ECategoryList>();
		new JSONAsyncTask().execute("http://128.199.176.163/labelkissapp/get_categories.php");
		
		ListView listview = (ListView)findViewById(R.id.list);
		
		adapter = new ECategoryAdapter(ECategories.this, R.layout.category_list_item, actorsList);

		listview.setAdapter(adapter);


		LayoutInflater inflater = getLayoutInflater();
		ViewGroup header = (ViewGroup) inflater.inflate(R.layout.category_header, listview,
				false);

		listview.addHeaderView(header, null, false);


		
		

	}


	class JSONAsyncTask extends AsyncTask<String, Void, Boolean> {
		
	ProgressDialog dialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			progress = (ProgressBar) findViewById(R.id.progressBar);
			Resources res = getResources();
			progress.setProgressDrawable(res.getDrawable(R.drawable.progress_bar));
			progress.setVisibility(View.VISIBLE);
			progress.setIndeterminate(true);
			progress.setProgress(0);
		}
		
		@Override
		protected Boolean doInBackground(String... urls) {
			try {
				
				//------------------>>
				HttpGet httppost = new HttpGet(urls[0]);
				HttpClient httpclient = new DefaultHttpClient();
				HttpResponse response = httpclient.execute(httppost);

				// StatusLine stat = response.getStatusLine();
				int status = response.getStatusLine().getStatusCode();

				if (status == 200) {
					HttpEntity entity = response.getEntity();
					String data = EntityUtils.toString(entity);
					
				
					jsono = new JSONObject(data);
					jarray = jsono.getJSONArray("cat");
					
					for (int i = 0; i < jarray.length(); i++) {
						JSONObject object = jarray.getJSONObject(i);

						ECategoryList actor = new ECategoryList();
						
						actor.setName(object.getString("catname"));
						actor.setImage(object.getString("catimages"));
						actor.setcatid(object.getString("catid"));
						
						
						actorsList.add(actor);
					}
					return true;
				}
				
				//------------------>>
				
			} catch (ParseException e1) {
				e1.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return false;
		}
		
		protected void onPostExecute(Boolean result) {
			progress.setProgress(100);
			adapter.notifyDataSetChanged();
			progress.setVisibility(View.GONE);
			if(result == false)
				Toast.makeText(ECategories.this, "Oopss!! Our engineer fell asleep,Please try again Later", Toast.LENGTH_LONG).show();

		}

	}



	
	
	
}
